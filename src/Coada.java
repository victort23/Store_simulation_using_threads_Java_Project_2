import java.util.LinkedList;
import java.util.Queue;

public class Coada implements Runnable{
    private LinkedList<Client> clienti;
    private LinkedList<Client> Aux;
    private int nr;
    private boolean busy=true;
    int i;
    private int totalWaitingTime;
        public Coada(int nr)
        {
            clienti=new LinkedList<>();
            Aux=new LinkedList<>();
            busy=false;
            totalWaitingTime=0;
            this.nr=nr;
        }
        public void addClient(Client p)
        {
            clienti.add(p);
            totalWaitingTime+= p.gettService();
            busy=true;
        }
        public void delClient()
        {
            Client removed;
            removed=clienti.remove();
            totalWaitingTime-= removed.gettService();
            if(clienti.size()==0)
                busy=false;
        }

    public int getTotalWaitingTime() {
        return totalWaitingTime;
    }

    @Override
    public void run() {


        while (busy==true) {
            if(clienti.size()>0) {
                clienti.get(0).settService(clienti.get(0).gettService() - 1);
                if (clienti.get(0).gettService() == 0) {
                    if (clienti.size() > 1) {
                        for (i = 1; i < clienti.size(); i++)
                            Aux.add(clienti.get(i));

                        clienti.clear();
                        for (i = 0; i < Aux.size(); i++)
                            clienti.add(Aux.get(i));
                        Aux.clear();

                    } else {
                        clienti.clear();

                    }
                }
                this.totalWaitingTime--;
            }
            else {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public String PrintQueue()
    {
        String s="Queue "+Integer.toString(nr)+" : ";
        if(busy==false)
        {
            s+=" closed";
            //System.out.println(s+"\n");
        }
        else
        {
            for(i=0;i<clienti.size();i++)
            {
                s+=clienti.get(i).toString();
                if(i!=clienti.size()-1)
                    s+=",";
            }
           // System.out.println(s+"\n");
        }
        return s;
    }

    public void sleep(int i) {
    }
}
