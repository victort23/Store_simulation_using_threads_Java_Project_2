public class Client implements Comparable{
    private int ID;
    private int tArrival;
    private int tService;

    public Client(int ID,int tArrival,int tService)
    {
        this.ID=ID;
        this.tArrival=tArrival;
        this.tService=tService;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int gettArrival() {
        return tArrival;
    }

    public void settArrival(int tArrival) {
        this.tArrival = tArrival;
    }

    public int gettService() {
        return tService;
    }

    public void settService(int tService) {
        this.tService = tService;
    }
    public String toString() {
        return "(" + ID + ", " + tArrival + ", " + tService + ")";
    }
    @Override
    public int compareTo(Object o) {
        if(this.tArrival < ((Client) o).tArrival) return -1;
        else if(this.tArrival > ((Client) o).tArrival) return 1;
        else return 0;
    }
}
