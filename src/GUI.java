import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.lang.management.RuntimeMXBean;
import java.util.LinkedList;

public class GUI extends JFrame {
    JFrame frameRez = new JFrame ("HELLO");
    JFrame frameSim = new JFrame ("Simulation");
   public JPanel p = new JPanel();

    JLabel nrCT = new JLabel(" Numarul de clienti ");
    JLabel arrMinT = new JLabel(" tArrival min ");
    JLabel arrMaxT = new JLabel(" tArrival max ");
    JLabel servMinT = new JLabel(" tService min ");
    JLabel servMaxT = new JLabel(" tService max ");
    JLabel nrCozi = new JLabel("Nr de cozi: ");
    JTextField nrCoziT = new JTextField(3);
    JLabel sim = new JLabel("Simulation time");
    JTextField nrC1 = new JTextField(3);
    JTextField arrMin = new JTextField(3);
    JTextField arrMax = new JTextField(3);
    JTextField servMin = new JTextField(3);
    JTextField servMax = new JTextField(3);
    JTextField simT = new JTextField(3);
    JPanel panelRez = new JPanel();
    public int OK=0;
    JButton start = new JButton("Start");
    LinkedList<Client> WaitList=new LinkedList<>();
    JLabel labelRez = new JLabel("Introduceti datele :");
    static String text;
        public GUI()
        {
            frameSim.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frameSim.setSize(600, 200);
            frameRez.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frameRez.setSize(600, 200);
            panelRez.setLayout(new FlowLayout());
            panelRez.add(labelRez);
            panelRez.add(nrCT);
            panelRez.add(nrC1);
            panelRez.add(arrMinT);
            panelRez.add(arrMin);
            panelRez.add(sim);
            panelRez.add(simT);
            panelRez.add(arrMaxT);
            panelRez.add(arrMax);
            panelRez.add(servMinT);
            panelRez.add(servMin);
            panelRez.add(servMaxT);
            panelRez.add(servMax);
            panelRez.add(nrCozi);
            panelRez.add(nrCoziT);
            panelRez.add(start);
            start.addActionListener(new java.awt.event.ActionListener(){
                public void actionPerformed(ActionEvent e){
                    JTextArea JT=new JTextArea();
                    JT.setLayout(new GridLayout());
                    String text2;
                    JScrollPane scrollableTextArea = new JScrollPane(JT);
                    scrollableTextArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
                    scrollableTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                    Thread t=new Thread(() -> {
                    text=arrMin.getText();
                    JLabel que=new JLabel();
                    Magazin.arrMin1=Integer.parseInt(text.trim());
                    text=arrMax.getText();
                    Magazin.arrMax1=Integer.parseInt(text.trim());
                    text=servMin.getText();
                    Magazin.servMin1=Integer.parseInt(text.trim());
                    text=servMax.getText();
                    JLabel clients=new JLabel();
                    Magazin.servMax1=Integer.parseInt(text.trim());
                    text=nrC1.getText();
                    Magazin.nrCl1=Integer.parseInt(text.trim());
                    text=simT.getText();
                    Magazin.SimT1=Integer.parseInt(text.trim());
                    text=nrCoziT.getText();
                    Magazin.nrQueues=Integer.parseInt(text.trim());
                    System.out.println(Magazin.nrQueues);
                    WaitList=Magazin.WaitingList(Magazin.nrCl1,Magazin.arrMax1,Magazin.arrMin1,Magazin.servMax1,Magazin.servMin1);
                    int nrClient = 1;
                        for (int i = 1; i <= Magazin.nrQueues; i++) {
                            Thread t1 = new Thread(Magazin.queues[i - 1]);
                            t1.start();
                        }
                    for(int timp = 0; timp <=Magazin.SimT1; timp++) {

                        Magazin.gchar.print("\nTime " + timp + "\n");
                        text = "Time : " + Integer.toString(timp) + '\n' ;
                        while(nrClient <= Magazin.nrCl1 && WaitList.get(nrClient - 1).gettArrival() <= timp) {
                            int celMaiMicTimp = Magazin.queues[0].getTotalWaitingTime(), selectieCoada = 0;
                            for(int i = 0; i < Magazin.nrQueues; i++) {
                                if(Magazin.queues[i].getTotalWaitingTime() < celMaiMicTimp) {
                                    celMaiMicTimp = Magazin.queues[i].getTotalWaitingTime();
                                    selectieCoada = i;
                                }
                            }
                            Magazin.queues[selectieCoada].addClient(WaitList.get(nrClient - 1));
                            Magazin.waitTimeAllQueues += Magazin.queues[selectieCoada].getTotalWaitingTime();
                            Magazin.avgServiceTime+=WaitList.get(nrClient - 1).gettService();
                            Magazin.peakHour += WaitList.get(nrClient - 1).gettArrival();
                            nrClient++;
                        }

                        System.out.println(".............");
                        Magazin.gchar.print(" Waiting clients: ");
                        text+=" Waiting clients: \n";
                        for(int i = nrClient; i < Magazin.nrCl1; i++)
                        {
                            text+=(WaitList.get(i - 1).toString() + ", ")+'\n';
                            Magazin.gchar.print(WaitList.get(i - 1).toString() + ", ");

                        }
                        if(nrClient != Magazin.nrCl1 + 1)
                        {
                            JLabel c=new JLabel(WaitList.get(Magazin.nrCl1 - 1).toString());
                            text+=WaitList.get(Magazin.nrCl1 - 1).toString()+"   "+'\n';
                            Magazin.gchar.print(WaitList.get(Magazin.nrCl1 - 1).toString());
                        }

                        Magazin.gchar.println();
                        for(int i = 1; i <=Magazin.nrQueues; i++)
                        {
                            System.out.println(Magazin.queues[i-1].PrintQueue());
                            text+=Magazin.queues[i-1].PrintQueue()+"  "+'\n';
                            Magazin.gchar.print(Magazin.queues[i-1].PrintQueue());
                        }
                        try {

                            JT.setText(text);
                            Thread.sleep(1000);
                            frameSim.getContentPane().add(scrollableTextArea);
                            frameSim.setVisible(true);

                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }

                    }

                    p.removeAll();
                    text=Double.toString(Magazin.waitTimeAllQueues/Magazin.nrCl1);
                    JLabel Avg=new JLabel("Average waiting time: "+ text);
                    p.add(Avg);
                    text=Double.toString(Magazin.avgServiceTime/Magazin.nrCl1);
                    JLabel AvgServ=new JLabel("Average service time: "+ text);
                    p.add(AvgServ);
                    text=Double.toString(Magazin.peakHour/Magazin.nrCl1);
                    JLabel Peak=new JLabel("Peak Hour: "+ text);
                    p.add(Peak);
                    Magazin.gchar.println("\n\nAverage waiting time: " + Magazin.waitTimeAllQueues/Magazin.nrCl1);
                    Magazin.gchar.println("\n\nAverage service time: " + Magazin.avgServiceTime/Magazin.nrCl1);
                    Magazin.gchar.println("\n\nPeak hour: " + Magazin.peakHour/Magazin.nrCl1);
                    frameSim.getContentPane().add(p);
                    frameSim.setVisible(true);
                });
                t.start();
        }
        });
       frameRez.setContentPane(panelRez);
       frameRez.setVisible(true);
        }
}
